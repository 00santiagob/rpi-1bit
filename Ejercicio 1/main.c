
//-------------------------------------------------------------------------
//-------------------------------------------------------------------------

#include "hal/rpi-gpio.h"
#include "hal/rpi-armtimer.h"
#include "hal/rpi-interrupts.h"

#define CANTIDAD_NOTAS 7 // Notas

extern void __enable_interrupts ( void );

//-------------------------------------------------------------------------
void __attribute__((interrupt("IRQ"))) interrupt_vector ( void )
{
    static int spk_on = 0;

    /* Notas musicales en useg */
    static int NOTAS[CANTIDAD_NOTAS] = {
        1911, 1703, 1517, 1432, 1275, 1136, 1012
    };
    
    /* Contadores para que las notas suenen por 5 seg */
    /* Formula = 5000000/nota */
    static int COUNTER[CANTIDAD_NOTAS] = {
        2616, 2935, 3295, 3491, 3921, 4401, 4940
    };
    
    static int count_down = 2616; // Contador para la primera nota.
    static int nota = 1; // Siguiente nota.

    /* Clear the ARM Timer interrupt - it's the only interrupt we have
       enabled, so we want don't have to work out which interrupt source
       caused us to interrupt */
    RPI_GetArmTimer()->IRQClear = 1;

    if (count_down == 0)
    {
        count_down = COUNTER[nota];
        RPI_GetArmTimer()->Reload = NOTAS[nota];
        nota = (nota + 1) % CANTIDAD_NOTAS;
    }

    if ( spk_on )
    {
        SPK_OFF();
        spk_on = 0;
        count_down--;
    }
    else
    {
        SPK_ON();
        spk_on  = 1;
        count_down--;
    }
}

//-------------------------------------------------------------------------
int notmain ( void )
{

    
    RPI_GetIrqController()->Disable_Basic_IRQs = RPI_BASIC_ARM_TIMER_IRQ;

    RPI_GetGpio()->SPK_GPFSEL |= 1 << SPK_GPFBIT; // Configurar GPIO del SPK como salida | Revisar definiciones en archivo rpi-gpio.h
    
    /* Setup the system timer interrupt */
    /* Timer frequency = CLK frequency = 1MHz, CLK period = 1 useg */
    RPI_GetArmTimer()->Load = 1911; // originally 1.000.000 useg = 1 seg.

    /* Setup the ARM Timer */
    RPI_GetArmTimer()->Control =
        RPI_ARMTIMER_CTRL_23BIT |
        RPI_ARMTIMER_CTRL_ENABLE |
        RPI_ARMTIMER_CTRL_INT_ENABLE |
        RPI_ARMTIMER_CTRL_PRESCALE_1;


    RPI_GetIrqController()->Enable_Basic_IRQs = RPI_BASIC_ARM_TIMER_IRQ;
    __enable_interrupts();
    
    while(1) {
    }
    return(0);
}
