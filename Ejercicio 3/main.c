
//-------------------------------------------------------------------------
//-------------------------------------------------------------------------

#include "hal/rpi-gpio.h"
#include "hal/rpi-armtimer.h"
#include "hal/rpi-interrupts.h"

#define CANTIDAD_NOTAS 12 // Notas
#define CANTIDAD_OCTAVAS 3

extern void __enable_interrupts ( void );

//-------------------------------------------------------------------------
void __attribute__((interrupt("IRQ"))) interrupt_vector ( void )
{
    static int spk_on = 0;

    /* Notas musicales en useg */
     /* Frecuencia de notas musicales en useg */
    static int NOTAS[CANTIDAD_OCTAVAS][CANTIDAD_NOTAS] = {
        {3822, 3608, 3405, 3214, 3034, 2863, 2703, 2551, 2408, 2273, 2145, 2025},
        {1911, 1801, 1703, 1607, 1517, 1432, 1351, 1275, 1204, 1136, 1121, 1012},
        { 956,  902,  851,  803,  758,  716,  676,  638,  602,  568,  536,  506}
    };
    
    /* Contadores para que las notas suenen por 5 seg */
    /* Formula = 5000000/nota */
    static int COUNTER[CANTIDAD_OCTAVAS][CANTIDAD_NOTAS] = {
        {1308, 1385, 1468, 1556, 1648, 1746, 1850, 1960, 2076, 2200, 2331, 2469},
        {2616, 2776, 2935, 3111, 3296, 3491, 3701, 3921, 4153, 4401, 4460, 4940},
        {5230, 5543, 5875, 6227, 6596, 6983, 7396, 7837, 8306, 8803, 9328, 9881}
    };
    
    static int count_down = 2616; // Contador para la primera nota.
    static int nota = 1; // Siguiente nota.
    static int octava = 1;

    /* Clear the ARM Timer interrupt - it's the only interrupt we have
       enabled, so we want don't have to work out which interrupt source
       caused us to interrupt */
    RPI_GetArmTimer()->IRQClear = 1;
    
    if (!(RPI_GetGpio()->GPIO_GPLEV &= (1 << SW_GPIO_BIT))){
        octava = (octava + 1) % CANTIDAD_OCTAVAS;
        count_down = 100;
    }

    if (count_down == 0)
    {
        count_down = COUNTER[octava][nota];
        RPI_GetArmTimer()->Reload = NOTAS[octava][nota];
        nota = (nota + 1) % CANTIDAD_NOTAS;
    }

    if ( spk_on )
    {
        SPK_OFF();
        spk_on = 0;
        count_down--;
    }
    else
    {
        SPK_ON();
        spk_on  = 1;
        count_down--;
    }
}

//-------------------------------------------------------------------------
int notmain ( void )
{

    
    RPI_GetIrqController()->Disable_Basic_IRQs = RPI_BASIC_ARM_TIMER_IRQ;

    RPI_GetGpio()->SW_GPFSEL  |= 0 << SW_GPIO_BIT; // Configuramos el Switch como entrada

    RPI_GetGpio()->SPK_GPFSEL |= 1 << SPK_GPFBIT; // Configurar GPIO del SPK como salida | Revisar definiciones en archivo rpi-gpio.h
    
    /* Setup the system timer interrupt */
    /* Timer frequency = CLK frequency = 1MHz, CLK period = 1 useg */
    RPI_GetArmTimer()->Load = 1911; // originally 1.000.000 useg = 1 seg.

    /* Setup the ARM Timer */
    RPI_GetArmTimer()->Control =
        RPI_ARMTIMER_CTRL_23BIT |
        RPI_ARMTIMER_CTRL_ENABLE |
        RPI_ARMTIMER_CTRL_INT_ENABLE |
        RPI_ARMTIMER_CTRL_PRESCALE_1;


    RPI_GetIrqController()->Enable_Basic_IRQs = RPI_BASIC_ARM_TIMER_IRQ;
    __enable_interrupts();
    
    while(1) {
    }
    return(0);
}
