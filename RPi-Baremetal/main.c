
//-------------------------------------------------------------------------
//-------------------------------------------------------------------------

#include "hal/rpi-gpio.h"
#include "hal/rpi-armtimer.h"
#include "hal/rpi-interrupts.h"

#define CANTIDAD_NOTAS 12 // Notas normales + las notas con sostenido.
#define CANTIDAD_OCTAVAS 3 // La 3, 4 y 5 octava.

#define NOTA_REDONDA     4000000 // 4 seg.
#define NOTA_BLANCA      2000000 // 2 seg.
#define NOTA_NEGRA       1000000 // 1 seg.
#define NOTA_CORCHEA     500000  // 0.5 seg.
#define NOTA_SEMICORCHEA 250000  // 0.25 seg.
#define NOTA_FUSA        125000  // 0.125 seg.
#define NOTA_SEMIFUSA    62500   // 0.0625 seg.

#define MICROPAUSA 6250

// Indice de la Nota en el arreglo NOTAS
#define C  0
#define Cs 1
#define D  2
#define Ds 3
#define E  4
#define F  5
#define Fs 6
#define G  7
#define Gs 8
#define A  9
#define As 10
#define B  11

extern void __enable_interrupts ( void );

//-------------------------------------------------------------------------
// Función auxiliar que nos ayuda a calcular el tiempo en que cada nota debe durar
int div_for_tempo(int tempo, int nota) {
    int res = 0;
    for(int count=nota; count<=tempo; count+=nota){
        res++;
    }
    return res;
}
// ------------------------------------------------------------------------

void __attribute__((interrupt("IRQ"))) interrupt_vector ( void )
{
    static int spk_on = 0;
    
    /* Frecuencia de notas musicales en useg */
    static int NOTAS[CANTIDAD_OCTAVAS][CANTIDAD_NOTAS] = {
        {3822, 3608, 3405, 3214, 3034, 2863, 2703, 2551, 2408, 2273, 2145, 2025},
        {1911, 1801, 1703, 1607, 1517, 1432, 1351, 1275, 1204, 1136, 1121, 1012},
        { 956,  902,  851,  803,  758,  716,  676,  638,  602,  568,  536,  506}
    };

    static const char *LA_CUCARACHA[192] = {
        "PPn", "c4s", "PPP", "c4s","PPP", "c4s",
        "f4c", "MAc", "a4s", "MAc", "c4s", "PPP", "c4s", "PPP", "c4s",
        "f4c", "MAc", "a4s", "MAn", "PPc",
        "f4c", "f4s", "e4s", "PPP", "e4s", "R4s", "PPP", "R4s",
        "c4n", "c4s", "PPP", "c4s", "PPP", "c4s",
        "e4c", "MAc", "g4s", "MAc", "c4s", "PPP", "c4s", "PPP", "c4s",
        "e4c", "MAc", "g4s", "MAn", "PPc",
        "c5c", "d5s", "c5s", "A4s", "a4s", "g4s",
        "a4n", "c4s", "PPP", "c4s", "PPP", "c4s",
        "f4c", "MAc", "a4s", "MAc", "c4s", "PPP", "c4s", "PPP", "c4s",
        "f4c", "MAc", "a4s", "MAn", "PPc",
        "f4c", "f4s", "e4s","PPP", "e4s", "R4s", "PPP", "R4s",
        "c4n", "c4s","PPP", "c4s", "PPP", "c4s",
        "e4c", "MAc", "g4s", "MAc", "c4s", "PPP", "c4s", "PPP", "c4s",
        "e4c", "MAc", "g4s", "MAn", "PPc",
        "c5c", "d5s", "c5s", "A4s", "a4s", "g4s",
        "f4n", "PPn",
        "c4c", "c4s", "f4s", "PPP", "f4s", "a4s", "PPP", "a4s",
        "c5c", "MAc", "a4n", "MAn",
        "c5c", "d5s", "c5s", "A4s", "a4s", "c5s",
        "A4c", "MAc", "g4n", "MAc",
        "c4c", "c4s", "e4s", "PPP","e4s", "g4s", "PPP","g4s",
        "A4c", "MAc", "g4n", "MAc",
        "c5c", "d5s", "c5s", "A4s", "a4s", "g4s",
        "a4b",
        "c4c", "c4s", "f4s", "PPP","f4s", "a4s","PPP", "a4s",
        "c5c", "MAc", "a4n", "MAn",
        "c5c", "d5s", "c5s", "A4s", "a4s", "c5s",
        "A4c", "MAc", "g4n", "MAc",
        "c4c", "c4s", "e4s", "PPP","e4s", "g4s", "PPP","g4s",
        "A4c", "MAc", "g4n", "MAc",
        "c5c", "d5s", "c5s", "A4s", "a4s", "g4s",
        "f4b"
    };

    // Inicializamos las variables que utilizamos
    static int octava = 1;
    static int nota = 0;
    static int count_down = 261;
    static int tempo = NOTA_CORCHEA; //

    static int nt = 2; // Se usa para recorrer toda la cancion.
    int is_nota = 1; // Variable candado para controlar si es una nota o una pausa.

    /* Clear the ARM Timer interrupt - it's the only interrupt we have
       enabled, so we want don't have to work out which interrupt source
       caused us to interrupt */
    RPI_GetArmTimer()->IRQClear = 1;

    if(count_down == 0){   
        // Cálculo de Nota
        switch(LA_CUCARACHA[nt][0]){
            case 'c':
                nota = C;
                is_nota = 1;
                break; 
            case 'd':
                nota = D;
                is_nota = 1;
                break; 
            case 'e':
                nota = E;
                is_nota = 1;
                break; 
            case 'f':
                nota = F;
                is_nota = 1;
                break;
            case 'g':
                nota = G;
                is_nota = 1;
                break; 
            case 'a':
                nota = A;
                is_nota = 1;
                break; 
            case 'b':
                nota = B;
                is_nota = 1;
                break;
            case 'C':
                nota = Cs;
                is_nota = 1;
                break; 
            case 'D':
                nota = Ds;
                is_nota = 1;
                break; 
            case 'F':
                nota = Fs;
                is_nota = 1;
                break;
            case 'G':
                nota = Gs;
                is_nota = 1;
                break; 
            case 'A':
                nota = As;
                is_nota = 1;
                break;
            case 'P':
                is_nota = 0;
                break;
            case 'M':
                is_nota = 1;
            default:
                break;
        }
        // Cálculo de octava
        switch(LA_CUCARACHA[nt][1]){
            case '3':
                octava = 0;
                break; 
            case '4':
                octava = 1;
                break; 
            case '5':
                octava = 2;
                break; 
            default:
                break;
        }
        // Cálculo de tiempo que debe durar
        switch(LA_CUCARACHA[nt][2]){
            case 'r':
                tempo = NOTA_REDONDA;
                break;
            case 'b':
                tempo = NOTA_BLANCA;
                break;
            case 'n':
                tempo = NOTA_NEGRA;
                break;
            case 'c':
                tempo = NOTA_CORCHEA;
                break;
            case 's':
                tempo = NOTA_SEMICORCHEA;
                break;
            default:
                tempo = MICROPAUSA;
                break;
        }

        nt = (nt + 1) % 192;

        if(is_nota){
            RPI_GetArmTimer()->Reload = NOTAS[octava][nota];
            count_down = div_for_tempo(tempo, NOTAS[octava][nota]);
        }
        else{
            RPI_GetArmTimer()->Reload = 1;
            count_down = div_for_tempo(tempo, 1);
        }
    }

    if(spk_on){
        SPK_OFF();
        spk_on = 0;
        count_down--;
    }
    else{
        SPK_ON();
        spk_on  = 1;
        count_down--;
    }
}

//-------------------------------------------------------------------------
int notmain ( void )
{

    
    RPI_GetIrqController()->Disable_Basic_IRQs = RPI_BASIC_ARM_TIMER_IRQ;

    RPI_GetGpio()->SPK_GPFSEL |= 1 << SPK_GPFBIT; // Configurar GPIO del SPK como salida | Revisar definiciones en archivo rpi-gpio.h
    
    /* Setup the system timer interrupt */
    /* Timer frequency = CLK frequency = 1MHz, CLK period = 1 useg */
    RPI_GetArmTimer()->Load = 1911; // 1.000.000 useg = 1 seg.

    /* Setup the ARM Timer */
    RPI_GetArmTimer()->Control =
        RPI_ARMTIMER_CTRL_23BIT |
        RPI_ARMTIMER_CTRL_ENABLE |
        RPI_ARMTIMER_CTRL_INT_ENABLE |
        RPI_ARMTIMER_CTRL_PRESCALE_1;


    RPI_GetIrqController()->Enable_Basic_IRQs = RPI_BASIC_ARM_TIMER_IRQ;
    __enable_interrupts();
    
    while(1) {
        //============================ CODE APP HERE =====================================
    }
    return(0);
}
