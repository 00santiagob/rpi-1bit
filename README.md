RPI-1bit
==

## Arquitectura de Computadora: 2019

### 1-bit con Raspberry Pi

### Alumnos:
  * __Balog Santiago Alberto__ (@00santiagob) ~ _santiagobalog1998@gmail.com_
  * __Farias Guillermo Joel__ (@joelfarias999) ~ _joelfarias999@gmail.com_
  * __Santiago Bustamante__ (@bustacab) ~ _jsantiagobustamante@outlook.com_

## Resumen

Utilizando el manejo de _puertos I/O_ e interrupciones, aplicados a la programacion de procesadores _ARMv8_, utilizar una plataforma real (Raspberry Pi 3/3+) y hacer uso de bloques perifericos (**GPIO**) y **ARM Timer** para generar un respuesta audible controlada con un pin, con el cual se controla el cono del parlante en forma directa mediante una secuencia de 1’s y 0’s (tecnica conocida como _**1-bit audio**_).

## Contexto

Una onda de sonido se produce cuando un parlante es excitado por una senial electrica alternante en el tiempo, es decir que el flujo de corriente electrica en su bobinado cambia de sentido a lo largo del tiempo.

[Figura 1 - Esquema de excitacion de un parlante.](img/Fig-1.png)

Cuando la corriente electrica fluye en el sentido +/- (semiciclo positivo de la onda senoidal de la Fig. 2), el cono del parlante avanza y genera una onda de presion de aire. En cambio si la corriente fluye con direccion -/+ (semiciclo negativo de la onda senoidal de la Fig. 2), el cono retrocede y genera una onda de depresion de aire.

[Figura 2 - Ondas periodicas, (sup) Onda Senoidal e (inf) Onda Cuadrada.](img/Fig-2.png)


La distancia o recorrido del cono del parlante es proporcional a la amplitud de la senial que excita el parlante, determinando el volumen del sonido generado. Mientras que la cantidad de veces que el parlante avanza y retrocede en un periodo de tiempo dado determina el tono del sonido. Normalmente este periodo de tiempo es 1 seg, y esta velocidad de cambio (o frecuencia) se mide entonces en Hertz (Hz).

frec(Hz) = 1/T(seg) o T(seg) = 1/frec(Hz)

Este proyecto esta basado en la tecnica de generacion de sonido conocida como “1-bit audio” y hace referencia a la limitacion de solo poder generar una onda cuadrada de amplitud fija para excitar el parlante (ver onda inferior de Fig 2). De esta forma, si la senial de control del parlante es:

        “1” → parlante avanza
        ”0” → parlante retrocede

En esta tecnica solo es posible alterar el ritmo del cambio de la senial, pero no su amplitud, es decir, solo podemos controlar su frecuencia.

La variedad de tonos que nuestro oido es capaz de percibir es muy elevada, estando acotada tan solo por los limites de sensibilidad de nuestro sistema auditivo, normalmente desde los 20Hz hasta los 20kHz. La gama usual de frecuencias de los sonidos musicales es considerablemente mas pequeña que la gama audible, siendo el tono mas alto de un piano el de frecuencia 13.186kHz, valor que podemos considerar como limite superior de los tonos
fundamentales.

Para este proyecto utilizaremos las notas musicales :

| Nota (Espaniol) | Nota (Ingles) |
|----------------|--------------:|
| DO             | C             |
| DO# = REb      | C# = Db       |
| RE             | D             |
| RE# = MIb      | D# = Eb       |
| MI             | E             |
| FA             | F             |
| FA# = SOLb     | F# = Gb       |
| SOL            | G             |
| SOL# = LAb     | G# = Ab       |
| LA             | A             |
| LA# = SIb      | A# = Bb       |
| SI             | B             |

Con sus respectivas frecuencias en las distintas octavas:  

| Nota | Frecuencia(Hz) | Segundos(seg)  | Microsegundos(useg)/2 |
|------|----------------|----------------|----------------------:|
| C3   | 130.81         | 0.007644675    | 3822                  | 
| C#3  | 138.59         | 0,007215528    | 3608                  | 
| D3   | 146.83         | 0,006810597    | 3405                  | 
| D#3  | 155.56         | 0.006428388    | 3214                  | 
| E3   | 164.81         | 0.006067593    | 3034                  | 
| F3   | 174.61         | 0.005727049    | 2863                  | 
| F#3  | 185.00         | 0.005405405    | 2703                  | 
| G3   | 196.00         | 0.005102041    | 2551                  | 
| G#3  | 207.65         | 0.004815796    | 2408                  | 
| A3   | 220.00         | 0.004545455    | 2273                  | 
| A#3  | 233.08         | 0.004290372    | 2145                  | 
| B3   | 246.94         | 0.004049567    | 2025                  | 
| C4   | 261.63         | 0.003822192    | 1911                  | 
| C#4  | 277.63         | 0.003601916    | 1801                  | 
| D4   | 293.66         | 0.003405299    | 1703                  |  
| D#4  | 311.13         | 0.003214091    | 1607                  |  
| E4   | 329.63         | 0.003033704    | 1517                  |  
| F4   | 349.23         | 0.002863442    | 1432                  |  
| F#4  | 369.99         | 0.002702776    | 1351                  |  
| G4   | 392.00         | 0.002551020    | 1275                  | 
| G#4  | 415.30         | 0.002407898    | 1204                  | 
| A4   | 440.00         | 0.002272727    | 1136                  |  
| A#4  | 466.00         | 0.002242152    | 1121                  |  
| B4   | 493.88         | 0.002024783    | 1012                  |  
| C5   | 523.25         | 0.001911132    | 956                   | 
| C#5  | 554.37         | 0.001803849    | 902                   | 
| D5   | 587.33         | 0.001702620    | 851                   | 
| D#5  | 622.25         | 0.001607071    | 803                   | 
| E5   | 659.26         | 0.001516852    | 758                   | 
| F5   | 698.46         | 0.001431721    | 716                   | 
| F#5  | 739.99         | 0.001351370    | 676                   | 
| G5   | 783.99         | 0.001275526    | 638                   | 
| G#5  | 830.61         | 0.001203934    | 602                   | 
| A5   | 880.00         | 0.001136364    | 568                   | 
| A#5  | 932.33         | 0.001072582    | 536                   | 
| B5   | 987.77         | 0.001012381    | 506                   | 

> **Nota:**
> La nota de referencia del sistema musical occidental es actualmente el “LA” a 440Hz (porque la Organizacion Internacional de Estandarizacion asi lo fijo en 1955). A partir de esta nota de referencia se calculan las frecuencias que deben tener el resto de notas para que todos los instrumentos suenen correctamente afinados.

## Instrucciones

Solo modificar __main.c__ de la carpeta _RPI-Baremetal/_, para escribir un programa en lenguaje C.

Las definiciones contenidas en los archivos header (.h) de las librerias de hardware (HAL) pueden ser modificadas para adaptarlas a las necesidades de conexion de las GPIO de cada trabajo.

Compilar el programa haciendo:

        $ make clean
        $ make

Se generara un archivo __kernel7.img__ el cual se debe cargar en una SD o microSD formateada en FAT32 junto a los archivos de booteo para Raspberry Pi B/B+ de la carpeta _boot-RPI-para-B+_.

> **Nota:**
> Instalar el __GNU Aarch64 toolchain__.
> Quienes usan _Ubuntu_ pueden hacerlo con: `sudo apt install gcc-arm-none-eabi`.

> **Importante:**
> Habia un error en la direccion base de los registros del GPIO, esto se corrigio reemplazando las direcciones por:
> 
> * Peripheral Base Address = 0x3F000000
> * GPIO Offset Address = 0x200000
>
> De esta forma la direccion base del primer registro del bloque de los GPIO (correspondiente al registro GPFSEL0) esta dada por:
>
> * Peripheral Base Address + GPIO Offset Address = 0x3F200000

El programa debe habilitar el/los puertos a utilizar y configurarlos como salida (en el caso del parlante) y como entrada (el pulsador SW). Para ello se debe identificar los capos **FSELn** (donde "**n**" corresponde al numero del GPIO que se desea configurar) de 3 bits c/u, sabiendo que:

* 000 = entrada
* 001 = salida

Notar que cada registro GPFSELx permite configurar 10 puertos: GPFSEL0(del 0 al 9), GPFSEL1 (del 10 al 19), etc.

[Figura 3 - Raspberry Pi 3 pinout](img/Fig-3.png)

Poner "1" en una salida: utilizar el campo **SETn**, dentro del registro **GPSETx**, colocando 1 en los bits correspondientes a los GPIO que se desea encender.

Poner "0" en una salida: utilizar el campo **CLRn**, dentro del registro **GPCLRx**, colocando 1 en los bits correspondientes a los GPIO que se desea apagar.

Consultar el estado de una entrada para evaluar el SW: utilizar el campo ​ **LEVn**​, dentro del registro ​ **GPLEVx**​. Se debe leer el contenido del registro correspondiente y evaluar el contenido del bit LEVn de interés. De esta forma:

 * Si el bit LEVn = 0 -> SW pulsado.
 * Si el bit LEVn = 1 -> SW suelto.

> **Recordatorio:**
> Dejar el _bucle infinito_ para que el programa se ejecute mientras la Raspberry Pi permanezca encendida.
> Dejar una _linea vacia_ al final del codigo. El toolchain espera esta linea vacia para asegurarse de que el archivo realmente termino.

### Ejercicio 1

El programa debe generar una secuencia infinita de las notas DO - RE - MI - FA - SOL - LA - SI en la 4ta octava, cada nota debe reproducirse durante aproximadamente 5 segundos.

El control de tiempos del programa deben ser generados mediante la utilizacion del modulo ARM Timer por interrupcion. No se pueden utilizar delay generados por codigo.

### Ejercicio 2

El programa debe generar un efecto "llamativo" o melodia de duracion no menor a 30 segundos.

### Ejercicio 3

El programa debe generar al menos 3 melodias distintas de duracion no menor a 20 segundos c/u. Mediante la utilizacion de un pulsador conectado a un GPIO se debe poder controlar que melodia se reproduce. De esta forma, cada vez que se presione el pulsador (switch), se debe cambiar de melodia y pasar a la siguiente en forma ciclica. Si cualquier melodia llega a su fin antes de presionar el pulsador, debe comenzar nuevamente.
    
## Estructura del Proyecto

      rpi-1bit/
        |
        |__ boot-RPI-para-B+/
        |
        |__ RPI-Baremetal/
        |    |__ boot/
        |    |    |__ bootcode.bin
        |    |    |__ kernel7.img
        |    |    |__ README.md
        |    |    |__ start.elf
        |    |
        |    |__ hal/
        |    |    |__ rpi-armtimer.c
        |    |    |__ rpi-armtimer.h
        |    |    |__ rpi-base.h
        |    |    |__ rpi-gpio.c
        |    |    |__ rpi-gpio.h
        |    |    |__ rpi-interrupts.c
        |    |    |__ rpi-interrupts.h
        |    |
        |    |__ main.c
        |    |__ Makefile
        |    |__ memmap
        |    |__ RPI-Baremetal.cbp
        |    |__ vectors.s
        |
        |__ .gitignore
        |__ README.md
